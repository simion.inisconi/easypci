export interface TestCase {
  id: string;
  name: string;
  description: string;
  card: string;
  process: string;
}  

