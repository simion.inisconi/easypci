#include "ConfigManager.hpp"

using namespace std;

string ConfigManager::prefixesConfigFile = "prefixes.csv";
string ConfigManager::accountsConfigFile = "accounts.csv";
string ConfigManager::configurationFile = "config.ini";

bool ConfigManager::encryptionEnabled = false;
bool ConfigManager::wipePrefixData = false;
bool ConfigManager::wipeCardData = false;
bool ConfigManager::wipeTransactionData = false;
bool ConfigManager::wipeCardCheckerData = false;

void ConfigManager::loadConfig() {
    CSimpleIniA ini;
    SI_Error rc = ini.LoadFile(configurationFile.c_str());

    const char* servicesSection = "Encryption";
    const char* wipeSettingsSection = "WipeData";
    encryptionEnabled = ini.GetBoolValue(servicesSection, "enabled");
    wipePrefixData = ini.GetBoolValue(wipeSettingsSection, "wipe_prefix_data");
    wipeCardData = ini.GetBoolValue(wipeSettingsSection, "wipe_card_data");
    wipeCardCheckerData = ini.GetBoolValue(wipeSettingsSection, "wipe_card_checker_data");
    wipeTransactionData = ini.GetBoolValue(wipeSettingsSection, "wipe_transaction_data");
}


bool ConfigManager::isEncryptionEnabled() {
    return encryptionEnabled;
}

bool ConfigManager::shouldWipePrefixData() {
    return wipePrefixData;
}

bool ConfigManager::shouldWipeCardData() {
    return wipeCardData;
}

bool ConfigManager::shouldWipeCardCheckerData() {
    return wipeCardCheckerData;
}
bool ConfigManager::shouldWipeTransactionData() {
    return wipeTransactionData;
}