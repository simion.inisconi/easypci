#include "CardChecker.hpp"

CardChecker::CardChecker( const string& cardNumber,  const string& expirationDate,  const string& cvv, const string& network) : cardNumber(cardNumber), expirationDate(expirationDate), cvv(cvv), network(network)
{
    
}

CardChecker::~CardChecker()
{
    cleaner();
}

void CardChecker::cleaner() {
    if (ConfigManager::shouldWipeCardCheckerData()) {
        SensitiveDataHandler::secureWipe(cardNumber);
        SensitiveDataHandler::secureWipe(expirationDate);
        SensitiveDataHandler::secureWipe(cvv);
        SensitiveDataHandler::secureWipe(network);
    }
}
bool CardChecker::validateCard() {
    if (!checkExpirationDate()) {
        return false;
    }
    if (!checkCVV()) {
        return false;
    }
    if(!checkNetwork()) {
        return false;
    }
    return true;
}

bool CardChecker::checkExpirationDate() {
    if (this->expirationDate.empty()) {
        return false;
    }

    regex expDatePattern("^(0[1-9]|1[0-2])\\/([0-9]{2})$");
    if (!regex_match(this->expirationDate, expDatePattern)) {
        return false;
    }

    int month = stoi(this->expirationDate.substr(0, 2));
    int year = stoi(this->expirationDate.substr(3, 2));

    time_t timer = time(nullptr);
    tm* now = localtime(&timer);
    int currentYear = (now->tm_year + 1900) % 100;
    int currentMonth = now->tm_mon + 1;

    if (year < currentYear || (year == currentYear && month < currentMonth)) {
        return false;
    }
    
    return true;
}

bool CardChecker::checkCVV() {
    return !this->cvv.empty();
}


bool CardChecker::checkNetwork() {
    if (this->cardNumber.empty()) {
        return false;
    }

    char firstDigit = this->cardNumber[0];

    auto it = cardNetworkMap.find(firstDigit);
    if (it != cardNetworkMap.end()) {
        return it->second == this->network;
    }

    return false;
}