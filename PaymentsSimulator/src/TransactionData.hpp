#ifndef TRANSACTION_DATA_HPP
#define TRANSACTION_DATA_HPP

#include <string>
#include "CryptographicService.hpp"
#include "ConfigManager.hpp"
#include "SensitiveDataHandler.hpp"

using namespace std;

class TransactionData {
    public:
        TransactionData(const string& cardNumber, const string& expirationDate, const string& cvv, double amount, const string& currency, const string& operationCode);
        ~TransactionData();
        void cleaner();
        string getCardNumber() const;
        string getExpirationDate() const;
        string getCVV() const;
        double getAmount() const;
        string getCurrency() const;
        string getOperationCode() const;

    private:
        string cardNumber;
        string expirationDate;
        string cvv;
        string currency;
        string operationCode;
        double amount;
};
#endif