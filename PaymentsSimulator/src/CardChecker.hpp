#ifndef CARD_CHECKER_HPP
#define CARD_CHECKER_HPP

#include <string>
#include "ConfigManager.hpp"
#include "CryptographicService.hpp"
#include "SensitiveDataHandler.hpp"
#include <regex>

using namespace std;

class CardChecker {
public:
    CardChecker( const string& cardNumber,  const string& expirationDate, const string& cvv,  const string& network);
    ~CardChecker();
    void cleaner();
    bool validateCard();

private:
    bool checkExpirationDate();
    bool checkCVV();
    bool checkNetwork();
    string cardNumber;
    string cvv;
    string expirationDate;
    string network; 
    unordered_map<char, string> cardNetworkMap = {
        {'1', "Administrative"},
        {'4', "VISA"},
        {'5', "MasterCard"},
        {'3', "AMEX"}
    };
};

#endif