export interface SSHConnection {
    id: string;
    serverAddress: string;
    username: string;
    password: string;
    name: string;
  }
  