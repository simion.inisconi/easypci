#include <string>
#include "SensitiveDataHandler.hpp"
#include "ConfigManager.hpp"
#include "CryptographicService.hpp"

using namespace std;

class Card {
    public:
        Card(const string& number, const string& holder, const string& expirationDate, const string& cvv, const string& network);
        ~Card();
        void cleaner();
        string getNumber() const;
        string getHolder() const;
        string getExpirationDate() const;
        string getCVV() const;
        string getNetwork() const;

    private:
        string number;
        string expirationDate;
        string cvv;
        string network;
        string holder;
};


