#include "CryptographicService.hpp"

SecByteBlock CryptographicService::key;
SecByteBlock CryptographicService::iv;

void CryptographicService::initialize() {
    AutoSeededRandomPool prng;

    key = SecByteBlock(AES::DEFAULT_KEYLENGTH);
    prng.GenerateBlock(key, key.size());

    iv = SecByteBlock(AES::BLOCKSIZE);
    prng.GenerateBlock(iv, iv.size());
}

string CryptographicService::encrypt(const string& plainText) {
    string cipherText;

    AES::Encryption aesEncryption(key, AES::DEFAULT_KEYLENGTH);
    CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

    
    size_t blockSize = AES::BLOCKSIZE;
    size_t paddingSize = blockSize - (plainText.length() % blockSize);
    if (paddingSize == 0) {
        paddingSize = blockSize;
    }

    string paddedText = plainText;
    paddedText.append(paddingSize, static_cast<char>(paddingSize));

    StreamTransformationFilter stfEncryptor(cbcEncryption, new StringSink(cipherText));
    stfEncryptor.Put(reinterpret_cast<const unsigned char*>(paddedText.c_str()), paddedText.size());
    stfEncryptor.MessageEnd();

    return cipherText;
}

string CryptographicService::decrypt(const string& cipherText) {
    string decryptedText;

    AES::Decryption aesDecryption(key, AES::DEFAULT_KEYLENGTH);
    CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

    StreamTransformationFilter stfDecryptor(cbcDecryption, new StringSink(decryptedText));
    stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipherText.c_str()), cipherText.size());
    stfDecryptor.MessageEnd();

    size_t paddingSize = static_cast<size_t>(decryptedText[decryptedText.length() - 1]);
    decryptedText.resize(decryptedText.length() - paddingSize);

    return decryptedText;
}

  