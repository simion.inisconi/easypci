#include <iostream>
#include "Card.hpp"                  
#include "CardChecker.hpp"           
#include "TransactionData.hpp"       
#include "TransactionProcessor.hpp" 
#include "ConfigManager.hpp"
#include "CryptographicService.hpp"

using namespace std;

void displayMenu() {
    cout << "Select a transaction type:\n";
    cout << "1. Balance Inquiry\n";
    cout << "2. Cash Withdrawal\n";
    cout << "Enter your choice (1 or 2): ";
}

int main() {
    ConfigManager::loadConfig();
    CryptographicService::initialize();

    while (true) {
        displayMenu();
        int choice;
        cin >> choice;

        string cardNumber = SensitiveDataHandler::secureInput("Card number: ");
        string holder = SensitiveDataHandler::secureInput("Holder: ");
        string expirationDate = SensitiveDataHandler::secureInput("Expiration date: ");
        string cvv = SensitiveDataHandler::secureInput("CVV: ");
        string network = SensitiveDataHandler::secureInput("Network: ");

        if (ConfigManager::isEncryptionEnabled())
        {
            cardNumber = CryptographicService::decrypt(cardNumber);
            expirationDate = CryptographicService::decrypt(expirationDate);
            cvv = CryptographicService::decrypt(cvv);
            network = CryptographicService::decrypt(network);
        } 

        Card card(cardNumber, holder, expirationDate, cvv, network);

        SensitiveDataHandler::secureWipe(cardNumber);
        SensitiveDataHandler::secureWipe(holder);
        SensitiveDataHandler::secureWipe(expirationDate);
        SensitiveDataHandler::secureWipe(cvv);

        TransactionProcessor transactionProcessor;
        bool transactionResult = false;

    CardChecker cardChecker = CardChecker(card.getNumber(), card.getExpirationDate(), card.getCVV(), card.getNetwork());
        if (cardChecker.validateCard()) {

            switch (choice) {
                case 1: {
                    cout << "Performing balance inquiry...\n";
                    TransactionData transactionData(card.getNumber(), card.getExpirationDate(), card.getCVV(), 0.0, "USD", "BINQ");
                    transactionResult = transactionProcessor.processTransaction(transactionData);
                    transactionData.cleaner();
                    break;
                }
                case 2: {
                    cout << "Enter amount to withdraw: ";
                    double amount;
                    cin >> amount;
                    TransactionData transactionData(card.getNumber(), card.getExpirationDate(), card.getCVV(), amount, "USD", "WDL");
                    transactionResult = transactionProcessor.processTransaction(transactionData);
                    transactionData.cleaner();
                    break;
                }
                default:
                    cout << "Invalid choice. Please try again.\n";
                    continue;
            }

            if (transactionResult) {
                cout << "Transaction succeeded!\n";
            } else {
                cout << "Transaction failed.\n";
            }
        }
        else {
            cout << "INVALID CARD!" << endl;
            cardChecker.cleaner();
        }
        cardChecker.cleaner();
        card.cleaner();
        cout << "Do you want to perform another transaction? (y/n): ";
        char anotherTransaction;
        cin >> anotherTransaction;
        ConfigManager::loadConfig();
        if (anotherTransaction != 'y' && anotherTransaction != 'Y') {
            break;
        }
    }

    return 0;
}