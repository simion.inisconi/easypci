#ifndef CRYPTOGRAPHIC_SERVICE_HPP
#define CRYPTOGRAPHIC_SERVICE_HPP

#include <string>
#include <cryptopp/aes.h>
#include <cryptopp/filters.h>
#include <cryptopp/modes.h>
#include <cryptopp/osrng.h>


using namespace std;
using namespace CryptoPP;

class CryptographicService {
    public:
        static void initialize();
        static string encrypt(const string& plainText);
        static string decrypt(const string& cipherText);

    private:
        static SecByteBlock key;
        static SecByteBlock iv;
};

#endif