#ifndef SENSITIVE_DATA_HANDLER_HPP
#define SENSITIVE_DATA_HANDLER_HPP


#include <iostream>
#include <string>
#include <termios.h>
#include <unistd.h>

using namespace std;

class SensitiveDataHandler {
    public:
        static void secureWipe(string& str);
        static string secureInput(const string& prompt);
};

#endif