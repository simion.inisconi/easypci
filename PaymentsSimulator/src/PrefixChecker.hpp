#include <string>
#include <unordered_set>
#include "ConfigManager.hpp"
#include "CryptographicService.hpp"
#include <fstream>
#include <sstream>
#include <iostream>
#include "SensitiveDataHandler.hpp"
#include "ConfigManager.hpp"
using namespace std;

struct PrefixInfo {
    string prefix;
    string type;
};

class PrefixChecker {
public:
    PrefixChecker(string cardNumber);
    ~PrefixChecker();
    void cleaner();
    bool isOnUsTransaction() ;
    bool isNotOnUsTransaction() ;
    vector<PrefixInfo>& getPrefixes();
private:
    string cardNumber;
    string obtainedPrefix; 
    vector<PrefixInfo> prefixes;
    unordered_set<string> onUsPrefixes;
    unordered_set<string> notOnUsPrefixes;
    
    void loadPrefixes();
};

