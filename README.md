# EasyPCI



## Getting started

EasyPCI is a tool designed for checking the PCI compliance using a distributed architecture between a back-end developed in ASP.NET and an analyzer developed in Flask. The front-end of the application is developed in Angular. In addition to the tool, there is also a lightweight payments simulator developed in C++ which simulates cases of improper data handling in the payments industry.

The entire project can be found at the following linkL: https://gitlab.upt.ro/simion.inisconi/easypci.git



## Instructions to run the application

In order to run the front-end application, the user would need to go to the folder containing the front end and run the ng  serve command.

For the back-end project, the user should open the application in Visual Studio 2022 and perform a run from there. It is recommended to create a local database for the back-end.

The EasyPCI analyzer requires a python virtual environment in order to run, but it has the requirements.txt file in there. The best way of running is is to use the PyCharm IDE.

For the Payments Simulator, the user must create a virtual machine of any type of linux distribution, and install the crytopp and the simpleini libraries. The compilation will be done using CMake and in the build directory, a script for running the simulator with proper conditions can be found.