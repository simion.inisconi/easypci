#include "PrefixChecker.hpp"

void PrefixChecker::loadPrefixes() {
    ifstream file(ConfigManager::prefixesConfigFile);
    string line;
    while (getline(file, line)) {
        istringstream stream(line);
        string prefix, type;
        if (getline(stream, prefix, ',') && getline(stream, type)) {
            PrefixInfo prefixInfo;
            prefixInfo.prefix = prefix;
            prefixInfo.type = type;
            prefixes.push_back(prefixInfo);
        }
    }
}

PrefixChecker::PrefixChecker(string cardNumber) {
    this->cardNumber = cardNumber;
    this->obtainedPrefix = this->cardNumber.substr(0, 6);
    loadPrefixes();

    for ( auto& prefixInfo : prefixes) {
        if (prefixInfo.type == "on-us") {
            onUsPrefixes.insert(prefixInfo.prefix);
        } else if (prefixInfo.type == "not-on-us") {
            notOnUsPrefixes.insert(prefixInfo.prefix);
        }
    }
}

PrefixChecker::~PrefixChecker() {
    cleaner();
}

void PrefixChecker::cleaner() {
    if( ConfigManager::shouldWipePrefixData()) {
        SensitiveDataHandler::secureWipe(cardNumber);
        SensitiveDataHandler::secureWipe(obtainedPrefix);
    }
}

bool PrefixChecker::isOnUsTransaction() {
    if (onUsPrefixes.count(this->obtainedPrefix) > 0) {
        return true;
    }

    return false;
}

bool PrefixChecker::isNotOnUsTransaction()  {
    if (notOnUsPrefixes.count(this->obtainedPrefix) > 0) {
        return true;
    }

    return false;
}

vector<PrefixInfo>& PrefixChecker::getPrefixes() {
    return prefixes;
}