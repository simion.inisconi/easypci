#include "TransactionData.hpp"

TransactionData::TransactionData(const string& cardNumber, const string& expirationDate, const string& cvv, double amount, const string& currency, const string& operationCode)
    : cardNumber(cardNumber), expirationDate(expirationDate), cvv(cvv), amount(amount), currency(currency), operationCode(operationCode) {
    if (ConfigManager::isEncryptionEnabled()) {
        this->cardNumber = CryptographicService::decrypt(cardNumber);
        this->expirationDate = CryptographicService::decrypt(expirationDate);
        this->cvv = CryptographicService::decrypt(cvv);
    }
}

TransactionData::~TransactionData() {
    cleaner();
}

void TransactionData::cleaner() {
    if (ConfigManager::shouldWipeTransactionData()) {
        SensitiveDataHandler::secureWipe(cardNumber);
        SensitiveDataHandler::secureWipe(expirationDate);
        SensitiveDataHandler::secureWipe(cvv);
        SensitiveDataHandler::secureWipe(currency);
        SensitiveDataHandler::secureWipe(operationCode);
    }
}

string TransactionData::getCardNumber() const {
    return cardNumber;
}

string TransactionData::getExpirationDate() const {
    return expirationDate;
}

string TransactionData::getCVV() const {
    return cvv;
}

double TransactionData::getAmount() const {
    return amount;
}

string TransactionData::getCurrency() const {
    return currency;
}

string TransactionData::getOperationCode() const {
    return operationCode;
}