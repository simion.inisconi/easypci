#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include "ConfigManager.hpp"
#include "TransactionData.hpp"
#include "PrefixChecker.hpp"

using namespace std;

class TransactionProcessor {
    public:
        TransactionProcessor();
        ~TransactionProcessor();
        bool processTransaction(const TransactionData& transactionData);
        void loadAccounts();
        void saveAccounts();

    private:
        string accountsConfigFile;
        unordered_map<string, double> accountMap;
        double getAccountBalance(const string& accountNumber);
        void updateAccount(const string& cardNumber, double newBalance);
};
