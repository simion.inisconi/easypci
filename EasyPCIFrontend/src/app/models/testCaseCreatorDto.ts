import { CardDto } from "./cardDto";

export interface TestCaseCreatorDto {
  cards: CardDto[];
}