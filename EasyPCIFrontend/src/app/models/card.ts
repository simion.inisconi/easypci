export interface Card {
    id: string;
    cardNumber: string;
    cardType: string;
    cardHolder: string;
    expirationDate: string;
    cvvCode: string;
  }  