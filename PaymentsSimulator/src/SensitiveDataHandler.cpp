#include "SensitiveDataHandler.hpp"

void SensitiveDataHandler::secureWipe(string& str) {
    volatile char *p = const_cast<char*>(str.data());
    for (size_t i = 0; i < str.size(); ++i) {
        p[i] = 0;
    }
    str.clear();
}

string SensitiveDataHandler::secureInput(const string& prompt) {
    string input;
    struct termios oldt, newt;

    cout << prompt;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;

    newt.c_lflag &= ~ECHO;
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    cin >> input;

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

    cout << endl;

    return input;
}
