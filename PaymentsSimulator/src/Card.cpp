#include "Card.hpp"

Card::Card(const string& number, const string& holder, const string& expirationDate, const string& cvv, const string& network)
    : number(number), holder(holder), expirationDate(expirationDate), cvv(cvv), network(network)
{
    if (ConfigManager::isEncryptionEnabled()) {
        this->number = CryptographicService::encrypt(number);
        this->holder = CryptographicService::encrypt(holder);
        this->expirationDate = CryptographicService::encrypt(expirationDate);
        this->cvv = CryptographicService::encrypt(cvv);
        this->network = CryptographicService::encrypt(network);
    }
}

Card::~Card() {
    cleaner();
}

void Card::cleaner() {
    if (ConfigManager::shouldWipeCardData()) {
        SensitiveDataHandler::secureWipe(number);
        SensitiveDataHandler::secureWipe(holder);
        SensitiveDataHandler::secureWipe(expirationDate);
        SensitiveDataHandler::secureWipe(cvv);
        SensitiveDataHandler::secureWipe(network);
    }
}

string Card::getNumber() const {
    return number;
}

string Card::getHolder() const {
    return holder;
}

string Card::getExpirationDate() const {
    return expirationDate;
}

string Card::getCVV() const {
    return cvv;
}

string Card::getNetwork() const {
    return network;
}