export interface TestResult {
    id: string; 
    name: string;
    testCase: string; 
    remote: string;
    result: string[];
    linkToCore: string;
    tester: string;
}