export interface Test {
    name: string;
    testCase: string;
    remote: string;
    tester: string;
  }
  