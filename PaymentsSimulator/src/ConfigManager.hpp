#ifndef CONFIG_MANAGER_HPP
#define CONFIG_MANAGER_HPP

#include <string>
#include <vector>
#include <unordered_map>
#include "../include/simpleini/SimpleIni.h"
#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

class ConfigManager {
public:
    static string prefixesConfigFile;
    static string accountsConfigFile;
    static string configurationFile;
    
    static void loadConfig();

    static bool isEncryptionEnabled();
    static bool shouldWipePrefixData();
    static bool shouldWipeCardData();
    static bool shouldWipeCardCheckerData();
    static bool shouldWipeTransactionData();

private:
    
    static bool encryptionEnabled;
    static bool wipePrefixData;
    static bool wipeCardData;
    static bool wipeCardCheckerData;
    static bool wipeTransactionData;
};

#endif