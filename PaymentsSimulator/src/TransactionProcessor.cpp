#include "TransactionProcessor.hpp"

TransactionProcessor::TransactionProcessor() {
    loadAccounts();
}

TransactionProcessor::~TransactionProcessor() {
    saveAccounts();
}

bool TransactionProcessor::processTransaction(const TransactionData& transactionData) {
    PrefixChecker prefixChecker = PrefixChecker(transactionData.getCardNumber());
    
    if(prefixChecker.isOnUsTransaction()) {
        double currentBalance = getAccountBalance(transactionData.getCardNumber());
        
        if (transactionData.getOperationCode() == "BINQ") {
                cout << "Current balance: " << currentBalance << " " << transactionData.getCurrency() << "\n";
                return true;
        }
        else if (transactionData.getOperationCode() == "WDL") {
                if (transactionData.getAmount() > currentBalance) {
                    cout << "Insufficient funds.\n";
                    return false;
                }

                cout << "Withdrawing " << transactionData.getAmount() << " " << transactionData.getCurrency() << "\n";
                double newBalance = currentBalance - transactionData.getAmount();
                cout << "New balance: " << newBalance << "\n";

                updateAccount(transactionData.getCardNumber(), newBalance);

                return true;
            }
        else {
            cout << "UNKNOWN TRANSACTION!" << endl;
            return false;
        }
    }
    else if ( prefixChecker.isNotOnUsTransaction())
    {
        cout << "It's a not-on-us transaction, redirectering to the card network..." << endl;
    }
    else
    { 
        cout << "A prefix for this card could not be found, transaction refused!" << endl;
    }

    return false;
}

void TransactionProcessor::loadAccounts() {
    ifstream file(ConfigManager::accountsConfigFile);
    accountMap.clear();

    string line;
    while (getline(file, line)) {
        istringstream stream(line);
        string account, amountStr;
        if (getline(stream, account, ',') && getline(stream, amountStr)) 
            accountMap[account] = stod(amountStr);
    }

    file.close();
}

void TransactionProcessor::updateAccount(const string& cardNumber, double newBalance){
    accountMap[cardNumber.substr(6,7)] = newBalance;

}

void TransactionProcessor::saveAccounts() {
    ofstream file(ConfigManager::accountsConfigFile);

    for (const auto& account : accountMap) {
        file << account.first << "," << account.second << "\n";
    }

    file.close();
}

double TransactionProcessor::getAccountBalance(const string& cardNumber) {
    auto it = accountMap.find(cardNumber.substr(6, 7));
    if (it != accountMap.end()) {
        return it->second;
    }
    return 0.0; 
}
